# Services Provided

When you develop with centur.us, you will have powerful, yet simple services such as:

## Primary services

+ Credential Verification
+ Authorization Rule Trees
+ Instant Messaging building blocks
+ Static Image hosting with dynamic resize
+ Scheduled Events or callbacks
+ Job Queues
+ Event Dispatch
+ *And much, much, more!*

## Instrumentation services

+ Service-Instance request statistics
+ Rate Limiting
+ Health Checking and notifications

## Frustration services

To help build reliant applications, we optionally provide
a [chaos monkey][] which randomly rejects requests or
disables whole services for short amounts of time.

Don't end up like [Instagram and Vine][outage], prepare
and embrace failure.
Never let failure be an exception to providing your users with the best possible experience!




*More anticipated features will be added soon.*

[chaos monkey]: http://www.codinghorror.com/blog/2011/04/working-with-the-chaos-monkey.html
[outage]: http://www.zdnet.com/amazon-web-services-suffers-outage-takes-down-vine-instagram-flipboard-with-it-7000019842/