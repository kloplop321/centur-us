#What is centur.us?

With centur.us, you can build social software from [micro-services][mu-s] managed by centur.us.
These building blocks, or micro-services, will give you the building blocks to build applications that are:

+ Secure
+ Performant
+ Dependable

[mu-s]: http://davidmorgantini.blogspot.com/2013/08/micro-services-what-are-micro-services.html