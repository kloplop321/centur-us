# Explorable API

Like Github, centur.us will be easy to explore for developers.

![Github API](/assets/images/api.png)

[Rest principles][rest] are fundamental to centur.us's design, making it scalable not just for servers, but for developers too!

Our API is discoverable, allowing you to make future-proof applications against our API.
Try out [jq][] against github to see what we mean.

[rest]: http://en.wikipedia.org/wiki/Representational_state_transfer
[jq]: http://stedolan.github.io/jq/