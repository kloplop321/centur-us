lrSnippet = require("grunt-contrib-livereload/lib/utils").livereloadSnippet
mountFolder = (connect, dir) ->
  connect.static require("path").resolve(dir)

module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")
    sass:
      dev:
        options:
          style: "expanded"
          lineNumbers: true
          trace: true

        files:
          "static/assets/css/_main.css": "sass/main.scss"
    jade:
      compile:
        options:
          pretty: true

        files:
          "static/index.html": "jade/index.jade"

    coffee:
      compile:
        options:
          bare: true

        files:
          "static/assets/js/_main.js": "coffee/main.coffee"

    regarde:
      less:
        files: ["sass/*", "sass/capsule/*"]
        tasks: ["sass", "livereload"]

      jade:
        files: ["jade/*.jade", "jade/templates/*"]
        tasks: ["jade", "livereload"]

      markdown:
        files: ["markdown/*.md", "markdown/*.markdown"]
        tasks: ["jade", "livereload"]

      coffee:
        files: ["coffee/*"]
        tasks: ["coffee", "livereload"]

    connect:
      
      #server: {
      #				options: {
      #					port: 8192,
      #					base: 'static'
      #				}	
      #			}
      livereload:
        options:
          port: 8192
          base: "static"
          middleware: (connect, options) ->
            [lrSnippet, mountFolder(connect, options.base)]

  grunt.loadNpmTasks "grunt-contrib-livereload"
  grunt.loadNpmTasks "grunt-contrib-connect"
  grunt.loadNpmTasks "grunt-regarde"
  grunt.loadNpmTasks "grunt-contrib-sass"
  grunt.loadNpmTasks "grunt-contrib-jade"
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.registerTask "default", ["sass", "jade", "coffee", "livereload-start", "connect", "regarde"]